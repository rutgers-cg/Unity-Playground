﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Curve : MonoBehaviour {

    public enum CURVE_TYPE {
        QUADRATIC_BEZIER,
        HERMITE
    }

    public CURVE_TYPE Type;
    public bool DrawControlPoints = true;
    public int Steps = 10;
    public float CurvePointsRadius = 0.1f;
    public Vector3[] ControlPoints;

    private void OnDrawGizmos() {

        Vector3 prev = ControlPoints[0];

        for (int i = 1; i < Steps; ++i) {
            float t = (i / (float) Steps);
            Vector3 curPoint = GetCurvePoint(t);
            Gizmos.DrawLine(prev, curPoint);
            prev = curPoint;
        }

        if(Type == CURVE_TYPE.HERMITE)
            Gizmos.DrawLine(prev, ControlPoints[3]);
    }

    public Vector3 GetCurvePoint(float t) {

        Vector3 point = Vector3.zero;

        switch (Type) {
            case CURVE_TYPE.QUADRATIC_BEZIER:
                if (ControlPoints.Length % 3 == 0) {
                    Vector3
                        p0 = ControlPoints[0],
                        p1 = ControlPoints[1],
                        p2 = ControlPoints[2];

                    float oneminus = (1 - t);

                    Vector3 p01 = oneminus * p0 + t * p1;
                    Vector3 p12 = oneminus * p1 + t * p2;

                    point = oneminus * p01 + t * p12;
                }
                break;
            case CURVE_TYPE.HERMITE:
                if(ControlPoints.Length == 4) {
                    float oneMinus = (1 - t); 
                    point =  Mathf.Pow(oneMinus, 3) * ControlPoints[0] +
                        3f * Mathf.Pow(oneMinus,2) * t * ControlPoints[1] +
                        3f * oneMinus * Mathf.Pow(t,2) * ControlPoints[2] + 
                        Mathf.Pow(t,3) * ControlPoints[3];
                }
                break;
        }

        return point;
    }

}
