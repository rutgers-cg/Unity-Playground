﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Tests : MonoBehaviour {

    public Transform Agent;
    public Transform TravelPoint;

	// Use this for initialization
	void Start () {
        NavMeshAgent navAgent = Agent.GetComponent<NavMeshAgent>();
        navAgent.SetDestination(TravelPoint.position);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
