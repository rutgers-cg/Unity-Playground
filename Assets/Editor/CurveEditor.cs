﻿
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Curve))]
public class CurveEditor : Editor {

    
    private void OnSceneGUI() {
        
        Curve curve = (Curve) target;
        Transform mainHandleTransform = curve.transform;
        
        if (curve.DrawControlPoints) {
            Handles.color = Color.white;
            for (int i = 1; i < curve.ControlPoints.Length; ++i) {
                EditorGUI.BeginChangeCheck();
                Vector3 worldPoint = mainHandleTransform.TransformPoint(curve.ControlPoints[i]);
                Vector3 currentLocal = Handles.DoPositionHandle(worldPoint, mainHandleTransform.rotation);
                Handles.DrawLine(mainHandleTransform.TransformPoint(curve.ControlPoints[i - 1]), worldPoint);
                if(EditorGUI.EndChangeCheck()) {
                    // make changes undoabled
                    Undo.RecordObject(curve, "Control Point Move");
                    EditorUtility.SetDirty(curve);
                    //
                    // Update Control Point position in World Coordinates
                    curve.ControlPoints[i] = mainHandleTransform.InverseTransformPoint(currentLocal);
                }
            }
        }
        

    }

}
